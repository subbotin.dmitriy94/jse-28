package com.tsconsulting.dsubbotin.tm.util;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.ParseException;
import java.util.Date;

public class DateAdapter extends XmlAdapter<String, Date> {

    @Override
    public String marshal(Date v) {
        return SystemUtil.getSimpleDateFormat().format(v);
    }

    @Override
    public Date unmarshal(String v) throws ParseException {
        return SystemUtil.getSimpleDateFormat().parse(v);
    }

}