package com.tsconsulting.dsubbotin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;

@UtilityClass
public final class SystemUtil {

    private final static String PATTERN_DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";

    private final static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(PATTERN_DATE_FORMAT);

    public long getPID() {
        @Nullable final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName == null || processName.isEmpty()) return 0;
        try {
            return Long.parseLong(processName.split("@")[0]);
        } catch (@NotNull final Exception e) {
            return 0;
        }
    }

    public static String getPatternDateFormat() {
        return PATTERN_DATE_FORMAT;
    }

    public SimpleDateFormat getSimpleDateFormat() {
        return SIMPLE_DATE_FORMAT;
    }

}
