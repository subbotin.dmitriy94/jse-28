package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IAuthRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IAuthService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyLoginException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyPasswordException;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@RequiredArgsConstructor
public final class AuthService implements IAuthService {

    @NotNull
    private final IAuthRepository authRepository;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Override
    @NotNull
    public String getCurrentUserId() throws AbstractException {
        @Nullable final String id = authRepository.getCurrentUserId();
        if (EmptyUtil.isEmpty(id)) throw new AccessDeniedException();
        return id;
    }

    @Override
    public void setCurrentUserId(@NotNull final String id) {
        authRepository.setCurrentUserId(id);
    }

    @Override
    public void login(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(login)) throw new EmptyLoginException();
        if (EmptyUtil.isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final User user = userService.findByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        @NotNull final String hash = getHash(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() throws AbstractException {
        @Nullable final String id = authRepository.getCurrentUserId();
        if (EmptyUtil.isEmpty(id)) throw new AccessDeniedException();
        authRepository.setCurrentUserId(null);
    }

    @Override
    public void registry(@NotNull final String login,
                         @NotNull final String password,
                         @NotNull final String email) throws AbstractException {
        userService.create(login, password, Role.USER, email);
    }

    @Override
    @NotNull
    public User getUser() throws AbstractException {
        @Nullable final String id = authRepository.getCurrentUserId();
        if (EmptyUtil.isEmpty(id)) throw new AccessDeniedException();
        return userService.findById(id);
    }

    @Override
    public void checkRoles(@Nullable final Role @Nullable ... roles) throws AbstractException {
        if (roles == null || roles.length == 0) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        for (@Nullable final Role item : roles) {
            if (item == null || item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    private String getHash(@NotNull String password) {
        return HashUtil.salt(propertyService.getPasswordIteration(), propertyService.getPasswordSecret(), password);
    }

}
