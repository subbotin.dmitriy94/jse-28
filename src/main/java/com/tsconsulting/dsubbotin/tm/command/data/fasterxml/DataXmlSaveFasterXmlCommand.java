package com.tsconsulting.dsubbotin.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class DataXmlSaveFasterXmlCommand extends AbstractFasterXmlDataCommand {

    @Override
    public @NotNull String name() {
        return "data-save-xml-fasterxml";
    }

    @Override
    public @NotNull String description() {
        return "Data saved in XML using fasterxml.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new XmlMapper());
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        @NotNull final File file = new File(FILE_XML_FASTERXML);
        @NotNull final Domain domain = getDomain();
        objectMapper.writeValue(file, domain);
        TerminalUtil.printMessage("Save to XML completed.");
    }

}
