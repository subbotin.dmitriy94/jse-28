package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserIsAuthCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-auth";
    }

    @Override
    @NotNull
    public String description() {
        return "Display authorized user.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

}
