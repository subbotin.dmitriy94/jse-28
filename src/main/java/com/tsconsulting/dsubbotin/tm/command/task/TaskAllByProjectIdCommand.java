package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class TaskAllByProjectIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-list-by-project-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Display task list by project id.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().findAllTasksByProjectId(currentUserId, projectId);
        int index = 1;
        for (@NotNull final Task task : tasks) TerminalUtil.printMessage(index++ + ". " + task.toString());
    }

}
