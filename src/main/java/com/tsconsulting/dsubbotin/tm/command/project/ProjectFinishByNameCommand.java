package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-finish-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Finish project by name.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().finishByName(currentUserId, name);
        TerminalUtil.printMessage("[Project complete]");
    }

}
