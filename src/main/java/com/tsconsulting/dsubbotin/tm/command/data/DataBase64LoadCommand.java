package com.tsconsulting.dsubbotin.tm.command.data;

import com.tsconsulting.dsubbotin.tm.dto.Domain;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-base64";
    }

    @Override
    public @NotNull String description() {
        return "Load base64 data.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final byte[] base64Data = Files.readAllBytes(Paths.get(FILE_BASE64));
        @NotNull final byte[] bytesData = Base64.getDecoder().decode(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytesData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

}
