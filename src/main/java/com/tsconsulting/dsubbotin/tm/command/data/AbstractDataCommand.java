package com.tsconsulting.dsubbotin.tm.command.data;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDomainException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    protected static final String FILE_XML_FASTERXML = "./data_fasterxml.xml";

    @NotNull
    protected static final String FILE_JSON_FASTERXML = "./data_fasterxml.json";

    @NotNull
    protected static final String FILE_XML_JAXB = "./data_jaxb.xml";

    @NotNull
    protected static final String FILE_JSON_JAXB = "./data_jaxb.json";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain) throws AbstractException {
        if (domain == null) throw new EmptyDomainException();
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        TerminalUtil.printMessage("Load completed.");
        serviceLocator.getAuthService().logout();
        TerminalUtil.printMessage("Logged out. Please log in.");
    }

    @Override
    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
