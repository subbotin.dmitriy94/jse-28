package com.tsconsulting.dsubbotin.tm.command.data.jaxb;

import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.Marshaller;
import java.io.File;

public final class DataXmlSaveJaxbCommand extends AbstractJaxbDataCommand {

    @Override
    public @NotNull String name() {
        return "data-save-xml-jaxb";
    }

    @Override
    public @NotNull String description() {
        return "Data saved in XML using JAXB.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Marshaller jaxbMarshaller = getJaxbMarshaller();
        @NotNull final File file = new File(FILE_XML_JAXB);
        @NotNull final Domain domain = getDomain();
        jaxbMarshaller.marshal(domain, file);
        TerminalUtil.printMessage("Save to XML completed.");
    }

}
