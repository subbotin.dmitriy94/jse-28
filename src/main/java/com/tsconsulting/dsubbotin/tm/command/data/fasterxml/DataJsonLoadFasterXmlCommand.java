package com.tsconsulting.dsubbotin.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class DataJsonLoadFasterXmlCommand extends AbstractFasterXmlDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-json-fasterxml";
    }

    @Override
    public @NotNull String description() {
        return "Data loaded from JSON using fasterxml.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new JsonMapper());
        @NotNull final File file = new File(FILE_JSON_FASTERXML);
        @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
    }

}
