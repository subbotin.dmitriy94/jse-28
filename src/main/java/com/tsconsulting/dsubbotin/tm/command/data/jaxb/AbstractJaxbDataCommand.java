package com.tsconsulting.dsubbotin.tm.command.data.jaxb;

import com.tsconsulting.dsubbotin.tm.command.data.AbstractDataCommand;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public abstract class AbstractJaxbDataCommand extends AbstractDataCommand {

    public Unmarshaller getJaxbUnmarshaller() throws JAXBException {
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(
                new Class[]{Domain.class},
                null);
        @NotNull final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return jaxbUnmarshaller;
    }

    public Marshaller getJaxbMarshaller() throws JAXBException {
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(
                new Class[]{Domain.class},
                null);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        return jaxbMarshaller;
    }

}
