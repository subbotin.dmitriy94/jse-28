package com.tsconsulting.dsubbotin.tm.command.user.admin;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "User remove by login.";
    }

    @Override
    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws Exception {
        TerminalUtil.printMessage("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        TerminalUtil.printMessage(String.format("%s user removed.", login));
    }

}
