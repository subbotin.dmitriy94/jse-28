package com.tsconsulting.dsubbotin.tm.command.data.jaxb;

import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.oxm.MediaType;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.Marshaller;
import java.io.File;

public final class DataJsonSaveJaxbCommand extends AbstractJaxbDataCommand {

    @Override
    public @NotNull String name() {
        return "data-save-json-jaxb";
    }

    @Override
    public @NotNull String description() {
        return "Data saved in JSON using JAXB.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Marshaller jaxbMarshaller = getJaxbMarshaller();
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
        @NotNull final File file = new File(FILE_JSON_JAXB);
        @NotNull final Domain domain = getDomain();
        jaxbMarshaller.marshal(domain, file);
        TerminalUtil.printMessage("Save to JSON completed.");
    }

}
