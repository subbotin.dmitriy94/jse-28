package com.tsconsulting.dsubbotin.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tsconsulting.dsubbotin.tm.command.data.AbstractDataCommand;
import com.tsconsulting.dsubbotin.tm.util.SystemUtil;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractFasterXmlDataCommand extends AbstractDataCommand {

    public ObjectMapper getObjectMapper(ObjectMapper objMapper) {
        @NotNull final ObjectMapper objectMapper = objMapper;
        objectMapper.setDateFormat(SystemUtil.getSimpleDateFormat());
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        return objMapper;
    }

}
