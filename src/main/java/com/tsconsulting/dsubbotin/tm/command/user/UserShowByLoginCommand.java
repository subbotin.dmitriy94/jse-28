package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserShowByLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-show-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "Display user by login.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws Exception {
        TerminalUtil.printMessage("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        showUser(user);
    }

}
