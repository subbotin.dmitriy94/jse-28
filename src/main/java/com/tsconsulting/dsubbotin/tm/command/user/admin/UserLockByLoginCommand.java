package com.tsconsulting.dsubbotin.tm.command.user.admin;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserLockByLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "User lock by login.";
    }

    @Override
    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws Exception {
        TerminalUtil.printMessage("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockByLogin(login);
        TerminalUtil.printMessage(String.format("%s user locked.", login));
    }

}
