package com.tsconsulting.dsubbotin.tm.command.data;

import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;

public final class DataBinSaveCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-save-bin";
    }

    @Override
    public @NotNull String description() {
        return "Save binary data.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        Files.delete(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final OutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        TerminalUtil.printMessage("Save to binary completed.");
        objectOutputStream.close();
        fileOutputStream.close();
    }

}
