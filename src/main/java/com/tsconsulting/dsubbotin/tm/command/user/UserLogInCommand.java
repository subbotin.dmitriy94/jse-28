package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserLogInCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-login";
    }

    @Override
    @NotNull
    public String description() {
        return "User log in.";
    }

    @Override
    public void execute() throws Exception {
        TerminalUtil.printMessage("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        TerminalUtil.printMessage("Logged in.");
    }

}
