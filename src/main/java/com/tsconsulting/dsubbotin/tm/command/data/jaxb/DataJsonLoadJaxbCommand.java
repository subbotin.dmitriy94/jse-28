package com.tsconsulting.dsubbotin.tm.command.data.jaxb;

import com.tsconsulting.dsubbotin.tm.dto.Domain;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.eclipse.persistence.oxm.MediaType;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataJsonLoadJaxbCommand extends AbstractJaxbDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-json-jaxb";
    }

    @Override
    public @NotNull String description() {
        return "Data loaded from JSON using JAXB.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Unmarshaller jaxbUnmarshaller = getJaxbUnmarshaller();
        jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
        @NotNull final File file = new File(FILE_JSON_JAXB);
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);
        setDomain(domain);
    }

}
